

# Getting started

Most of the works in this repository, especially the `R` scripts, should
be directly reproducible. You’ll need
[`git`](https://git-scm.com/downloads),
[`R`](https://www.r-project.org/),
[`quarto`](https://quarto.org/docs/download/), and more conveniently
[RStudio IDE](https://posit.co/downloads/) installed and running well in
your system. You simply need to fork/clone this repository using RStudio
by following [this tutorial, start right away from
`Step 2`](https://book.cds101.com/using-rstudio-server-to-clone-a-github-repo-as-a-new-project.html#step---2).
Using terminal in linux/MacOS, you can issue the following command:

``` bash
quarto tools install tinytex
```

This command will install `tinytex` in your path, which is required to
compile quarto documents as latex/pdf. Afterwards, in your RStudio
command line, you can copy paste the following code to setup your
working directory:

``` r
install.packages("renv") # Only need to run this step if `renv` is not installed
```

This step will install `renv` package, which will help you set up the
`R` environment. Please note that `renv` helps tracking, versioning, and
updating packages I used throughout the analysis.

``` r
renv::restore()
```

This step will read `renv.lock` file and install required packages to
your local machine. When all packages loaded properly (make sure there’s
no error at all), you *have to* restart your R session. At this point,
you need to export the data as `data.csv` and place it within the
`data/raw` directory. The directory structure *must* look like this:

``` bash
data
├── ...
├── raw
│   └── data.csv
└── ...
```

Then, you should be able to proceed with:

``` r
targets::tar_make()
```

This step will read `_targets.R` file, where I systematically draft all
of the analysis steps. Once it’s done running, you will find the
rendered document (either in `html` or `pdf`) inside the `draft`
directory.

# What’s this all about?

This is the functional pipeline for conducting statistical analysis. The
complete flow can be viewed in the following `mermaid` diagram:

``` mermaid
graph LR
  style Legend fill:#FFFFFF00,stroke:#000000;
  style Graph fill:#FFFFFF00,stroke:#000000;
  subgraph Legend
    direction LR
    x7420bd9270f8d27d([""Up to date""]):::uptodate --- xa8565c104d8f0705([""Dispatched""]):::dispatched
    xa8565c104d8f0705([""Dispatched""]):::dispatched --- xbf4603d6c2c2ad6b([""Stem""]):::none
    xbf4603d6c2c2ad6b([""Stem""]):::none --- xf0bce276fe2b9d3e>""Function""]:::none
    xf0bce276fe2b9d3e>""Function""]:::none --- x5bffbffeae195fc9{{""Object""}}:::none
  end
  subgraph Graph
    direction LR
    xc366d1d8806979f6>"genLabel"]:::uptodate --> xb4a3d4a2fc7bb7d2>"readData"]:::uptodate
    x97c70e20651b3e03>"genColor"]:::uptodate --> xb1a597158ed56c3a>"themePopCondition"]:::uptodate
    x97c70e20651b3e03>"genColor"]:::uptodate --> xab254314ddbef503>"themePopDomain"]:::uptodate
    x97c70e20651b3e03>"genColor"]:::uptodate --> x76d56032cf9edd22>"vizPop"]:::uptodate
    xb1a597158ed56c3a>"themePopCondition"]:::uptodate --> x76d56032cf9edd22>"vizPop"]:::uptodate
    x515150c62567fc27>"fetch"]:::uptodate --> x76d56032cf9edd22>"vizPop"]:::uptodate
    xab254314ddbef503>"themePopDomain"]:::uptodate --> x76d56032cf9edd22>"vizPop"]:::uptodate
    x69390d09f214dab8(["plt_pop_konu_0.25"]):::uptodate --> xba5363d05aae6782(["fig_pop_konu_0.25"]):::uptodate
    x0e2a1e9ee6890aa6>"saveFig"]:::uptodate --> xba5363d05aae6782(["fig_pop_konu_0.25"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x998bb11b6c3b3c56(["plt_pop_sos_0.05"]):::uptodate
    x76d56032cf9edd22>"vizPop"]:::uptodate --> x998bb11b6c3b3c56(["plt_pop_sos_0.05"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x8443c51921c34630(["plt_pop_kons_0.1"]):::uptodate
    x76d56032cf9edd22>"vizPop"]:::uptodate --> x8443c51921c34630(["plt_pop_kons_0.1"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> xd17711e1adb4cd37(["plt_pop_eko_0.1"]):::uptodate
    x76d56032cf9edd22>"vizPop"]:::uptodate --> xd17711e1adb4cd37(["plt_pop_eko_0.1"]):::uptodate
    xd17711e1adb4cd37(["plt_pop_eko_0.1"]):::uptodate --> xe866115dbdc60f53(["fig_pop_eko_0.1"]):::uptodate
    x0e2a1e9ee6890aa6>"saveFig"]:::uptodate --> xe866115dbdc60f53(["fig_pop_eko_0.1"]):::uptodate
    x998bb11b6c3b3c56(["plt_pop_sos_0.05"]):::uptodate --> x94d4819884f18615(["fig_pop_sos_0.05"]):::uptodate
    x0e2a1e9ee6890aa6>"saveFig"]:::uptodate --> x94d4819884f18615(["fig_pop_sos_0.05"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x69390d09f214dab8(["plt_pop_konu_0.25"]):::uptodate
    x76d56032cf9edd22>"vizPop"]:::uptodate --> x69390d09f214dab8(["plt_pop_konu_0.25"]):::uptodate
    x416b07d6ac4b1a97(["fpath"]):::uptodate --> xe797a538520c914d(["tbl"]):::uptodate
    xb4a3d4a2fc7bb7d2>"readData"]:::uptodate --> xe797a538520c914d(["tbl"]):::uptodate
    x80c83005db847782{{"raws"}}:::uptodate --> x416b07d6ac4b1a97(["fpath"]):::uptodate
    x8443c51921c34630(["plt_pop_kons_0.1"]):::uptodate --> xa41a81972b42ee3f(["fig_pop_kons_0.1"]):::uptodate
    x0e2a1e9ee6890aa6>"saveFig"]:::uptodate --> xa41a81972b42ee3f(["fig_pop_kons_0.1"]):::uptodate
    x6e52cb0f1668cc22(["readme"]):::dispatched --> x6e52cb0f1668cc22(["readme"]):::dispatched
    x2166607dfe6e75f2{{"funs"}}:::uptodate --> x2166607dfe6e75f2{{"funs"}}:::uptodate
    x2d15849e3198e8d1{{"pkgs"}}:::uptodate --> x2d15849e3198e8d1{{"pkgs"}}:::uptodate
    xea1cdd6f009574c2{{"pkgs_load"}}:::uptodate --> xea1cdd6f009574c2{{"pkgs_load"}}:::uptodate
    x6e87002190572330{{"seed"}}:::uptodate --> x6e87002190572330{{"seed"}}:::uptodate
    xfb5278c6bbbf3460>"lsData"]:::uptodate --> xfb5278c6bbbf3460>"lsData"]:::uptodate
    x9f4ef3011fe80273>"regularize"]:::uptodate --> x9f4ef3011fe80273>"regularize"]:::uptodate
  end
  classDef uptodate stroke:#000000,color:#ffffff,fill:#354823;
  classDef dispatched stroke:#000000,color:#000000,fill:#DC863B;
  classDef none stroke:#000000,color:#000000,fill:#94a4ac;
  linkStyle 0 stroke-width:0px;
  linkStyle 1 stroke-width:0px;
  linkStyle 2 stroke-width:0px;
  linkStyle 3 stroke-width:0px;
  linkStyle 30 stroke-width:0px;
  linkStyle 31 stroke-width:0px;
  linkStyle 32 stroke-width:0px;
  linkStyle 33 stroke-width:0px;
  linkStyle 34 stroke-width:0px;
  linkStyle 35 stroke-width:0px;
  linkStyle 36 stroke-width:0px;
```
